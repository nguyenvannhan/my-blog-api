<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Nguyen Van Nhan',
            'email' => 'nguyenvannhan0810@gmail.com',
            'password' => 'nvnhan@123456',
        ]);
    }
}
