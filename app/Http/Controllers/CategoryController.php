<?php

namespace App\Http\Controllers;

use App\Core\Businesses\Contracts\CategoryInterface as CategoryBusiness;
use App\Http\Requests\CreateCategoryRequest;
use App\Http\Requests\EditCategoryRequest;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    private $categoryBusiness;

    public function __construct(CategoryBusiness $categoryBusiness)
    {
        $this->categoryBusiness = $categoryBusiness;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('categories.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = $this->categoryBusiness->create();

        return view('categories.update', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCategoryRequest $request)
    {
        $data = $request->only(['name', 'parent_id', 'description']);

        $result = $this->categoryBusiness->store($data);

        if ($result['success']) {
            return redirect()->route('categories.index')->with(['updateCategory' => 'create']);
        }

        return back()->withInput()->withErrors(['common' => $result['messages']]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json([
            'category' => $this->categoryBusiness->show($id)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = $this->categoryBusiness->edit($id);

        return view('categories.update', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditCategoryRequest $request, $id)
    {
        $data = $request->only(['name', 'parent_id', 'description']);

        $result = $this->categoryBusiness->update($id, $data);

        if ($result['success']) {
            return redirect()->route('categories.index')->with(['updateCategory' => 'edit']);
        }

        return back()->withInput()->withErrors(['common' => $result['messages']]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return response()->json(
            $this->categoryBusiness->destroy($id)
        );
    }

    public function getTableData(Request $request)
    {
        return $this->categoryBusiness->getTableData($request);
    }
}
