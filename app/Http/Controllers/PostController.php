<?php

namespace App\Http\Controllers;

use App\Core\Businesses\Contracts\PostInterface as PostBusiness;
use App\Http\Requests\CreatePostRequest;
use App\Http\Requests\EditPostRequest;
use Illuminate\Http\Request;

class PostController extends Controller
{
    private $postBusiness;

    public function __construct(PostBusiness $postBusiness)
    {
        $this->postBusiness = $postBusiness;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('posts.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = $this->postBusiness->create();

        return view('posts.update', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePostRequest $request)
    {
        $data = $request->only(['title', 'category_ids', 'description', 'content', 'published_at', 'source_url', 'thumbnail']);

        $result = $this->postBusiness->store($data);

        if ($result['success']) {
            return redirect()->route('posts.index')->with(['updatePost' => 'create']);
        }

        return back()->withInput()->withErrors(['common' => $result['messages']]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json([
            'post' => $this->postBusiness->show($id)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = $this->postBusiness->edit($id);

        return view('posts.update', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditPostRequest $request, $id)
    {
        $data = $request->only(['title', 'category_ids', 'description', 'content', 'published_at', 'source_url', 'thumbnail']);

        $result = $this->postBusiness->update($id, $data);

        if ($result['success']) {
            return redirect()->route('posts.index')->with(['updatePost' => 'edit']);
        }

        return back()->withInput()->withErrors(['common' => $result['messages']]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return response()->json(
            $this->postBusiness->destroy($id)
        );
    }

    public function getTableData(Request $request)
    {
        return $this->postBusiness->getTableData($request);
    }
}
