<?php

namespace App\Http\Controllers\API;

use App\Core\Businesses\API\Contracts\PostInterface as PostBusiness;
use App\Http\Controllers\Controller;
use App\Http\Resources\Collection\PostCollection;
use App\Http\Resources\Resources\PostResource;
use Illuminate\Http\Request;

class PostController extends Controller
{
    private $postBusiness;

    public function __construct(PostBusiness $postBusiness)
    {
        $this->postBusiness = $postBusiness;
    }

    public function index(Request $request)
    {
        $options = [
            'paginate' => $request->page_size ?? 10,
        ];

        $categoryIds = $this->getCategoryIds($request->category_ids);

        $options = array_merge($request->except(['page_size', 'fields', 'category_ids']), $options);

        $postList = $this->postBusiness->index($categoryIds, $options);

        return new PostCollection($postList);
    }

    public function show($slug)
    {
        return new PostResource($this->postBusiness->show($slug));
    }

    private function getCategoryIds($categoryParam)
    {
        if (! $categoryParam) {
            return null;
        }

        if (is_array($categoryParam)) {
            return $categoryParam;
        }

        return [$categoryParam];
    }
}
