<?php

namespace App\Http\Controllers\API;

use App\Core\Businesses\API\Contracts\CategoryInterface as CategoryBusiness;
use App\Http\Controllers\Controller;
use App\Http\Resources\Collection\CategoryCollection;
use App\Http\Resources\Resources\CategoryResource;

class CategoryController extends Controller
{
    private $categoryBusiness;

    public function __construct(CategoryBusiness $categoryBusiness)
    {
        $this->categoryBusiness = $categoryBusiness;
    }

    public function index()
    {
        return new CategoryCollection($this->categoryBusiness->index());
    }

    public function show($slug)
    {
        return new CategoryResource($this->categoryBusiness->show($slug));
    }
}
