<?php

namespace App\Http\Controllers;

use App\Core\Businesses\Contracts\MediaInterface as MediaBusiness;
use App\Http\Requests\ImageUploadRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class MediaController extends Controller
{
    private $mediaBusiness;

    public function __construct(MediaBusiness $mediaBusiness)
    {
        $this->mediaBusiness = $mediaBusiness;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->mediaBusiness->index();

        return view('medias.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('medias.upload');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(ImageUploadRequest $request)
    {
        $data['file'] = $request->file('file');

        $result = $this->mediaBusiness->store($data);

        if (isset($result['success']) && $result['success']) {
            return redirect()->route('medias.index')->with('uploadGallery', 'upload');
        }

        return back()->withErrors($result['messages']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        return response()->json(
            $this->mediaBusiness->destroy($id)
        );
    }

    public function editorUpload(Request $request)
    {
        $request->validate([
            'editormd-image-file' => 'required|mimes:jpg,jpeg,svg,png,gif|max:10240'
        ]);

        $data['file'] = $request->file('editormd-image-file');

        $result = $this->mediaBusiness->store($data);

        $responseData = [
            'success' => false,
            'message' => 'Upload Image Fail',
            'url' => '',
        ];

        if (isset($result['success']) && $result['success']) {
            $responseData['success'] = true;
            $responseData['message'] = 'Upload Image Success';
            $responseData['url'] = $result['path'];
        }

        return response()->json($responseData);
    }
}
