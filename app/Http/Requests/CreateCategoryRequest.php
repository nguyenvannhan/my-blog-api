<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255|unique:categories',
            'description' => 'nullable',
            'parent_id' => 'nullable|numeric|exists:categories,id'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Name is required.',
            'name.string' => 'Name must be string.',
            'name.max' => 'Name is too long.',
            'name.unique' => 'Name is exists. Please change other name',
            'parent_id.numeric' => 'Parent Category must is wrong!',
            'parent_id.exists' => 'Parent Category is not exists',
        ];
    }
}
