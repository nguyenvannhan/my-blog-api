<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|max:255|unique:posts,title,' . Request()->post,
            'published_at' => 'nullable|date_format:Y-m-d',
            'source_url' => 'nullable',
            'content' => 'required_without:source_url',
            'category_ids' => 'nullable|array',
            'category_ids.*' => 'exists:categories,id',
            'thumbnail' => 'nullable|mimes:jpg,jpeg,png,gif,svg|max:2049|dimensions:width=600,ratio=3/2'
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Title is required',
            'title.string' => 'Title must be string',
            'title.max' => 'Title is too long',
            'title.unique' => 'Title is exists',
            'published_at.date_format' => 'Publish Date is wrong',
            'content.required_without' => 'Content is required when Source URL is empty',
            'category_ids.array' => 'Category value is wrong',
            'category_ids.*.exists' => 'Category value is wrong',
            'thumbnail.mimes' => 'Thumbnail file have extension is wrong!',
            'thumbnail.max' => 'Thumbnail file size too big',
            'thumbnail.dimensions' => 'Thumbnail Dimension is wrong. (Ratio: 3/2, Width: 600px)'
        ];
    }
}
