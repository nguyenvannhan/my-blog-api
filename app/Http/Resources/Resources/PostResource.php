<?php

namespace App\Http\Resources\Resources;

use App\Models\Media;
use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $thumbnailUrl = $this->getFirstMediaUrl(Media::GROUP_POST_THUMBNAIL);
        return [
            'id' => $this->id,
            'title' => $this->title,
            'slug' => $this->slug,
            'description' => $this->description,
            'thumbnail_url' => !empty($thumbnailUrl) ? $thumbnailUrl : config('media.no_image_url'),
            'content' => $this->content,
            'published_at' => $this->published_at->format('Y-m-d'),
            'categories' => $this->categories,
            'subjects' => $this->subjects,
        ];
    }
}
