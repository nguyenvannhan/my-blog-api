<?php

namespace App\Http\Resources\Collection;

use App\Http\Resources\Resources\PostResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class PostCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return PostResource::collection($this->collection);
    }
}
