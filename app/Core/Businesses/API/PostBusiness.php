<?php

namespace App\Core\Businesses\API;

use App\Core\Businesses\API\Contracts\PostInterface;
use App\Core\Repositories\Contracts\PostInterface as PostRepository;

class PostBusiness implements PostInterface
{
    private $postRepository;

    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    public function index($category_ids = null, $options = [])
    {
        if (isset($options['columns'])) {
            unset($options['columns']);
        }

        $options['with'] = ['categories', 'subjects'];

        if (!isset($options['orders']) || !empty($options['orders'])) {
            $options['orders'] = [
                ['published_at', 'DESC']
            ];
        }

        return $this->postRepository->apiGetList(
            $options['search'] ?? null,
            $category_ids,
            $options
        );
    }

    public function show($slug)
    {
        $request = Request();

        $columns = isset($request->fields) ? $request->fields : ['*'];

        if ($key = \array_search('categories', $columns) !== -1) {
            $options['with'] = ['categories'];

            unset($columns[$key]);
        }

        return $this->postRepository->findBySlug($slug, array_merge([
            'columns' => $columns,
        ], $request->all()));
    }
}
