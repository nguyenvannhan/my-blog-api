<?php

namespace App\Core\Businesses\API;

use App\Core\Businesses\API\Contracts\CategoryInterface;
use App\Core\Repositories\Contracts\CategoryInterface as CategoryRepository;

class CategoryBusiness implements CategoryInterface
{
    private $cateRepository;

    public function __construct(CategoryRepository $cateRepository)
    {
        $this->cateRepository = $cateRepository;
    }

    public function index()
    {
        $request = Request();

        return $this->cateRepository->getList(['*'], $request->all());
    }

    public function show($slug)
    {
        $request = Request();

        return $this->cateRepository->findBySlug($slug, $request->all());
    }
}
