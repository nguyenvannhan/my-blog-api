<?php

namespace App\Core\Businesses\API\Contracts;

interface PostInterface
{
    public function index($category_ids = null, $options = []);
}
