<?php

namespace App\Core\Businesses\API\Contracts;

interface CategoryInterface
{
    public function index();

    public function show($slug);
}
