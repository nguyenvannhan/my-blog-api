<?php

namespace App\Core\Businesses;

use App\Core\Businesses\Contracts\CategoryInterface;
use App\Core\Businesses\Contracts\DataTableInterface;
use App\Core\Businesses\Contracts\ResourceInterface;
use App\Core\Repositories\Contracts\CategoryInterface as CategoryRepository;
use App\ViewModels\CategoryUpdateViewModel;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class CategoryBusiness implements CategoryInterface, ResourceInterface, DataTableInterface
{
    private $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function index()
    {
        return null;
    }

    public function create()
    {
        $parentList = $this->categoryRepository->getList(['id', 'name']);

        return new CategoryUpdateViewModel(null, $parentList);
    }

    public function store($data)
    {
        if (! isset($data['parent_id']) || is_null($data['parent_id'])) {
            $data['parent_id'] = 0;
        }

        if ($this->categoryRepository->create($data)) {
            return ['success' => true];
        }

        return [
            'success' => false,
            'messages' => ['Create Category Failed!!!']
        ];
    }

    public function show($id)
    {
        return $this->categoryRepository->findOrFail($id, false, [], ['id', 'name', 'parent_id', 'description']);
    }

    public function edit($id)
    {
        $category = $this->categoryRepository->findOrFail($id);

        $parentList = $this->categoryRepository->getList(['id', 'name'], [
            'except_ids' => $category->id
        ]);

        return new CategoryUpdateViewModel($category, $parentList);
    }

    public function update($id, $data)
    {
        if (! isset($data['parent_id']) || is_null($data['parent_id'])) {
            $data['parent_id'] = 0;
        }

        if ($this->categoryRepository->update($id, $data)) {
            return [
                'success' => true,
            ];
        }

        return [
            'success' => false,
            'messages' => ['Update Category Failed!!!']
        ];
    }

    public function destroy($id)
    {
        $category = $this->categoryRepository->findOrFail($id);

        return $category->delete() ? ['success' => true] : ['success' => false];
    }

    public function getTableData(Request $request)
    {
        $categoriesQuery = $this->categoryRepository->getModelToQuery()->with(['categoryParent'])->withCount(['posts', 'subjects']);

        return DataTables::of($categoriesQuery)
        ->addColumn('parent', function ($category) {
            return $category->categoryParent ? $category->categoryParent->name : '';
        })
        ->addColumn('posts_subjects_count', function ($category) {
            return $category->posts_count . '/' . $category->subjects_count;
        })
        ->addColumn('action', function ($category) {
            return '<div class="text-center mb-2 d-inline"><a href="' . route('categories.edit', $category->id) . '" class="btn-edit  text-primary mx-1" title="Edit"><i class="fa fa-edit"></i></a></div>' .
            '<div class="text-center mb-2 d-inline"><a href="#" data-id="' . $category->id . '" class="btn-delete text-danger mx-1" title="Delete"><i class="fa fa-trash"></i></a></div>';
        })
        ->removeColumn('parent_id')
        ->rawColumns(['description', 'action'])
        ->make(true);
    }
}
