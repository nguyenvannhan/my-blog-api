<?php

namespace App\Core\Businesses\Contracts;

use Illuminate\Http\Request;

interface DataTableInterface
{
    public function getTableData(Request $request);
}
