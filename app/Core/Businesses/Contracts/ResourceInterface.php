<?php

namespace App\Core\Businesses\Contracts;

interface ResourceInterface
{
    public function index();

    public function create();

    public function store($data);

    public function show($id);

    public function edit($id);

    public function update($id, $data);

    public function destroy($id);
}
