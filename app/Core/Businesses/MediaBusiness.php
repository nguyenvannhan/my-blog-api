<?php

namespace App\Core\Businesses;

use App\Core\Businesses\Contracts\MediaInterface;
use App\Core\Businesses\Contracts\ResourceInterface;
use App\Core\Repositories\Contracts\MediaInterface as MediaRepository;

class MediaBusiness implements MediaInterface, ResourceInterface
{
    private $mediaRepository;

    public function __construct(MediaRepository $mediaRepository)
    {
        $this->mediaRepository = $mediaRepository;
    }

    public function index()
    {
        $mediaList = $this->mediaRepository->getList(['*'], [
            'paginate' => 20
        ]);

        return [
            'galleryList' => $mediaList
        ];
    }

    public function create()
    {
        return null;
    }

    public function store($data)
    {
        $media = $this->mediaRepository->upload($data['file']);

        if ($media) {
            return [
                'success' => true,
                'path' => $media->getUrl()
            ];
        }

        return [
            'success' => false,
            'messages' => ['common' => ['Upload Image Failed!']]
        ];
    }

    public function show($id)
    {
    }

    public function edit($id)
    {
    }

    public function update($id, $data)
    {
    }

    public function destroy($id)
    {
        $media = $this->mediaRepository->findOrFail($id);

        $media->loadCount(['posts']);

        if (! $media->posts_count) {
            $media->delete();

            return [
                'success' => true,
            ];
        }

        return [
            'success' => false,
        ];
    }
}
