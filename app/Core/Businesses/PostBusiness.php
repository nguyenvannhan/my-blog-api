<?php

namespace App\Core\Businesses;

use App\Core\Businesses\Contracts\DataTableInterface;
use App\Core\Businesses\Contracts\PostInterface;
use App\Core\Businesses\Contracts\ResourceInterface;
use App\Core\Repositories\Contracts\CategoryInterface as CategoryRepository;
use App\Core\Repositories\Contracts\MediaInterface as MediaRepository;
use App\Core\Repositories\Contracts\PostInterface as PostRepository;
use App\Models\Media;
use App\ViewModels\PostUpdateViewModel;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Yajra\DataTables\Facades\DataTables;

class PostBusiness implements PostInterface, ResourceInterface, DataTableInterface
{
    private $categoryRepository;
    private $mediaRepository;
    private $postRepository;

    public function __construct(CategoryRepository $categoryRepository, MediaRepository $mediaRepository, PostRepository $postRepository)
    {
        $this->categoryRepository = $categoryRepository;
        $this->mediaRepository = $mediaRepository;
        $this->postRepository = $postRepository;
    }

    public function index()
    {
        return null;
    }

    public function create()
    {
        $categoryList = $this->categoryRepository->getList(['id', 'name']);

        return new PostUpdateViewModel(null, $categoryList);
    }

    public function store($data)
    {
        $category_ids = Arr::pull($data, 'category_ids') ?? [];

        $post = $this->postRepository->create($data);

        if ($post) {
            $post->categories()->sync($category_ids);

            //Upload Media
            if (isset($data['thumbnail'])) {
                $media = $this->mediaRepository->upload($data['thumbnail']);

                $post->attachMedia($media, Media::GROUP_POST_THUMBNAIL);
            }

            return [
                'success' => true
            ];
        }

        return [
            'success' => false,
            'messages' => ['Create Post failed!']
        ];
    }

    public function show($id)
    {
        return $this->postRepository->findOrFail($id, false, ['categories', 'subjects']);
    }

    public function edit($id)
    {
        $post = $this->postRepository->findOrFail($id, false, ['categories', 'subjects']);

        $categoryList = $this->categoryRepository->getList(['id', 'name']);

        return new PostUpdateViewModel($post, $categoryList);
    }

    public function update($id, $data)
    {
        $category_ids = Arr::pull($data, 'category_ids') ?? [];

        $post = $this->postRepository->findOrFail($id);

        if ($post->update($data)) {
            $post->categories()->sync($category_ids);

            //Upload Media
            if (isset($data['thumbnail'])) {
                $media = $this->mediaRepository->upload($data['thumbnail']);

                //Check and remove old thumbnail
                $oldMedia = $post->getFirstMedia(Media::GROUP_POST_THUMBNAIL);
                if ($oldMedia) {
                    $post->detachMedia($oldMedia);
                }

                $post->attachMedia($media, Media::GROUP_POST_THUMBNAIL);
            }

            return [
                'success' => true
            ];
        }

        return [
            'success' => false,
            'messages' => ['Update Post failed!']
        ];
    }

    public function destroy($id)
    {
        $post = $this->postRepository->findOrFail($id);

        return $post->delete() ? ['success' => true] : ['success' => false];
    }

    public function getTableData(Request $request)
    {
        $postsQuery = $this->postRepository->getModelToQuery()->with(['categories']);

        return DataTables::of($postsQuery)->addColumn('categories', function ($post) {
            return $post->categories->implode('name', ', ');
        })
        ->addColumn('action', function ($post) {
            return '<div class="text-center mb-2 d-inline"><a href="' . route('posts.edit', $post->id) . '" class="btn-edit  text-primary mx-1" title="Edit"><i class="fa fa-edit"></i></a></div>' .
            '<div class="text-center mb-2 d-inline"><a href="#" data-id="' . $post->id . '" class="btn-delete text-danger mx-1" title="Delete"><i class="fa fa-trash"></i></a></div>';
        })
        ->removeColumn('description')
        ->rawColumns(['action'])
        ->make(true);
    }
}
