<?php

namespace App\Core\Repositories\Traits;

trait SlugQuery
{
    public function findBySlug($slug, $options = [])
    {
        $query = $this->getModelToQuery()->where('slug', $slug);

        return $this->buildQuery($query, array_merge($options, [
            'getFirst' => true,
        ]));
    }
}
