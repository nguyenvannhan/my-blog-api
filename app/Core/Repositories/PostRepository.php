<?php

namespace App\Core\Repositories;

use App\Core\Repositories\Contracts\PostInterface;
use App\Core\Repositories\Contracts\SlugInterface;
use App\Core\Repositories\Traits\SlugQuery;

class PostRepository extends BaseHelper implements PostInterface, SlugInterface
{
    use SlugQuery;

    public function getModel()
    {
        return \App\Models\Post::class;
    }


    /** API Section */

    public function apiGetList($keyword = null, $category_ids = [], $options = [])
    {
        $query = $this->getModelToQuery()->whereDate('published_at', '<=', now());

        if ($keyword) {
            $query = $query->where('title', 'LIKE', '%' . $keyword . '%')
                ->orWhere('description', 'LIKE', '%' . $keyword . '%')
                ->orWhereHas('categories', function ($categoryQuery) use ($keyword) {
                    $categoryQuery->where('categories.name', 'LIKE', '%' . $keyword . '%');
                });
        }

        if (!empty($category_ids)) {
            $query = $query->whereHas('categories', function ($categoryQuery) use ($category_ids) {
                $categoryQuery->whereIn('categories.id', $category_ids);
            });
        }

        return $this->buildQuery($query, $options);
    }
}
