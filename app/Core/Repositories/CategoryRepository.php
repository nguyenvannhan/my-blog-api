<?php

namespace App\Core\Repositories;

use App\Core\Repositories\Contracts\CategoryInterface;
use App\Core\Repositories\Contracts\SlugInterface;
use App\Core\Repositories\Traits\SlugQuery;

class CategoryRepository extends BaseHelper implements CategoryInterface, SlugInterface
{
    use SlugQuery;

    public function getModel()
    {
        return \App\Models\Category::class;
    }
}
