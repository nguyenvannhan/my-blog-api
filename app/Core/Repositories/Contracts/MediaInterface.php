<?php

namespace App\Core\Repositories\Contracts;

interface MediaInterface extends BaseInterface
{
    public function upload($file, $fileName = null, $name = null);
}
