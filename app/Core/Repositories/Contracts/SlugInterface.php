<?php

namespace App\Core\Repositories\Contracts;

interface SlugInterface
{
    public function findBySlug($slug, $options = []);
}
