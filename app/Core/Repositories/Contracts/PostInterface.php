<?php

namespace App\Core\Repositories\Contracts;

use Illuminate\Database\Eloquent\Collection;

interface PostInterface extends BaseInterface
{
    /**
     * Get Post list by categories
    * @param array $keyword
    * @param array $category_ids
    * @param array $options
    *
    * @return Collection
    */
    public function apiGetList($keyword = null, $category_ids = [], $options = []);
}
