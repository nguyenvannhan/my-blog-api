<?php

namespace App\Core\Repositories;

use App\Core\Repositories\Contracts\MediaInterface;
use Optix\Media\MediaUploader;

class MediaRepository extends BaseHelper implements MediaInterface
{
    public function getModel()
    {
        return \App\Models\Media::class;
    }

    public function upload($file, $fileName = null, $name = null)
    {
        $media = MediaUploader::fromFile($file);

        if (! empty($fileName)) {
            $media = $media->useFileName($fileName);
        } else {
            $media = $media->useFileName(date('Y-m-d') . '-' . \microtime(false) . '.' . $file->getClientOriginalExtension());
        }

        if (! empty($name)) {
            $media = $media->useName($name);
        }

        return $media->upload();
    }
}
