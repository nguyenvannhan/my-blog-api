<?php

namespace App\Core\Repositories;

use App\Core\Repositories\Contracts\SubjectInterface;

class SubjectRepository extends BaseHelper implements SubjectInterface
{
    public function getModel()
    {
        return \App\Models\Subject::class;
    }
}
