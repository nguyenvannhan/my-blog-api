<?php

namespace App\ViewModels;

use App\Models\Category;
use Illuminate\Support\Collection;
use Spatie\ViewModels\ViewModel;

class CategoryUpdateViewModel extends ViewModel
{
    private $category;
    private $parentList;

    public function __construct(?Category $category, Collection $parentList)
    {
        $this->category = $category;
        $this->parentList = $parentList;
    }

    public function category() : Category
    {
        $category = $this->category ?? new Category();

        if (old('name')) {
            $category->name = old('name');
        }
        if (old('parent_id')) {
            $category->parent_id = old('parent_id');
        }
        if (old('description')) {
            $category->description = old('description');
        }

        return $category;
    }

    public function parentList() : Collection
    {
        if ($this->parentList->count()) {
            return $this->parentList->prepend(new Category([
                'id' => null,
                'name' => null,
            ]));
        }

        return collect([]);
    }

    public function formInfo() : array
    {
        return ! $this->category ?
        [
            'method' => 'POST',
            'url' => route('categories.store'),
        ] :
        [
            'method' => 'PATCH',
            'url' => route('categories.update', $this->category->id),
        ];
    }

    public function pageTitle() : string
    {
        return $this->category ? 'Edit' : 'Create';
    }
}
