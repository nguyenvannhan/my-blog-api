<?php

namespace App\ViewModels;

use App\Models\Media;
use App\Models\Post;
use Illuminate\Support\Collection;
use Spatie\ViewModels\ViewModel;

class PostUpdateViewModel extends ViewModel
{
    private $post;
    private $categoryList;

    public function __construct(?Post $post, Collection $categoryList)
    {
        $this->post = $post;
        $this->categoryList = $categoryList;
    }

    public function post() : Post
    {
        $postItem = $this->post ?? new Post;

        if (old('title')) {
            $postItem->title = old('title');
        }

        if (old('category_ids')) {
            $postItem->category_ids = collect(old('category_ids'));
        } else {
            $postItem->category_ids = $postItem->categories->flatMap(function ($categoryItem) {
                return $categoryItem->id;
            });
        }

        if (old('description')) {
            $postItem->description = old('description');
        }

        if (old('content')) {
            $postItem->content = old('content');
        }

        if (old('published_at')) {
            $postItem->published_at = old('published_at');
        }

        if (old('source_url')) {
            $postItem->source_url = old('source_url');
        }

        $postItem->thumbnail_label = $postItem->getFirstMedia(Media::GROUP_POST_THUMBNAIL) ? $postItem->getFirstMediaUrl(Media::GROUP_POST_THUMBNAIL) : 'Choose file';

        return $postItem;
    }

    public function categoryList() : Collection
    {
        return $this->categoryList;
    }

    public function formInfo() : array
    {
        return $this->post ? [
            'method' => 'PATCH',
            'url' => route('posts.update', $this->post->id),
        ] : [
            'method' => 'POST',
            'url' => route('posts.store'),
        ];
    }

    public function pageTitle() : string
    {
        return $this->post ? 'Edit' : 'Create';
    }
}
