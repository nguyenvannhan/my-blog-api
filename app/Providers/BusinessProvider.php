<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class BusinessProvider extends ServiceProvider
{
    public $singletons = [
        \App\Core\Businesses\Contracts\CategoryInterface::class => \App\Core\Businesses\CategoryBusiness::class,
        \App\Core\Businesses\Contracts\MediaInterface::class => \App\Core\Businesses\MediaBusiness::class,
        \App\Core\Businesses\Contracts\PostInterface::class => \App\Core\Businesses\PostBusiness::class,
        \App\Core\Businesses\Contracts\SubjectInterface::class => \App\Core\Businesses\SubjectBusiness::class,

        \App\Core\Businesses\API\Contracts\CategoryInterface::class => \App\Core\Businesses\API\CategoryBusiness::class,
        \App\Core\Businesses\API\Contracts\PostInterface::class => \App\Core\Businesses\API\PostBusiness::class,
    ];

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
