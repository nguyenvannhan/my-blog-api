<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryProvider extends ServiceProvider
{
    public $singletons = [
        \App\Core\Repositories\Contracts\CategoryInterface::class => \App\Core\Repositories\CategoryRepository::class,
        \App\Core\Repositories\Contracts\MediaInterface::class => \App\Core\Repositories\MediaRepository::class,
        \App\Core\Repositories\Contracts\PostInterface::class => \App\Core\Repositories\PostRepository::class,
        \App\Core\Repositories\Contracts\SubjectInterface::class => \App\Core\Repositories\SubjectRepository::class,
    ];

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
