<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Optix\Media\HasMedia;

class Post extends Model
{
    use HasFactory;
    use HasMedia;
    use SoftDeletes;

    public static function booted()
    {
        static::creating(function ($post) {
            $post->fill([
                'author_id' => Auth::id(),
                'slug' => \make_slug($post->title)
            ]);
        });

        static::updating(function ($post) {
            $post->fill([
                'slug' => \make_slug($post->title)
            ]);
        });
    }

    protected $table = 'posts';

    protected $fillable = [
        'title', 'slug', 'description', 'content', 'source_url', 'published_at', 'author_id'
    ];

    protected $casts = [
        'published_at' => 'datetime:Y-m-d'
    ];

    /** Relationship n - 1 */
    public function author()
    {
        return $this->belongsTo('App\Models\User', 'author_id', 'id');
    }

    /** Relationship 1 - n */

    /** Relationship n - n */
    public function categories()
    {
        return $this->belongsToMany('App\Models\Category');
    }

    public function subjects()
    {
        return $this->belongsToMany('App\Models\Subject');
    }

    /** Scope */
    public function scopeOnlyPublished($query)
    {
        return $query->whereDate('published_at', '<=', now());
    }
}
