<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use HasFactory;
    use SoftDeletes;

    public static function booted()
    {
        static::creating(function ($category) {
            $category->fill([
                'slug' => \make_slug($category->name)
            ]);
        });

        static::updating(function ($category) {
            $category->fill([
                'slug' => \make_slug($category->name)
            ]);
        });
    }

    protected $table = 'categories';

    protected $fillable = [
        'name', 'slug', 'parent_id', 'description'
    ];

    /** Relationship n - 1 */
    public function categoryParent()
    {
        return $this->belongsTo('App\Models\Category', 'parent_id', 'id');
    }

    /** Relationship 1 - n */
    public function categoryChildren()
    {
        return $this->hasMany('App\Models\Category', 'parent_id', 'id');
    }

    /** Relationship n - n */
    public function posts()
    {
        return $this->belongsToMany('App\Models\Post');
    }

    public function subjects()
    {
        return $this->belongsToMany('App\Models\Subject');
    }
}
