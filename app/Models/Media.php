<?php

namespace App\Models;

use \Optix\Media\Models\Media as BaseMedia;

class Media extends BaseMedia
{
    const GROUP_POST_THUMBNAIL = 'post_thumbnail';

    public static function boot()
    {
        parent::boot();
        static::deleted(function ($model) {
            $model->filesystem()->deleteDirectory(
                $model->getDirectory()
            );
        });
    }

    public function posts()
    {
        return $this->morphedByMany('App\Models\Post', 'mediable');
    }
}
