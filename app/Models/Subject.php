<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    use HasFactory;

    protected $table = 'subjects';

    protected $fillable = [
        'name', 'description'
    ];

    /** Relationship n - 1 */

    /** Relationship 1 - n */

    /** Relationship n - n */
    public function categories()
    {
        return $this->belongsToMany('App\Models\Category');
    }

    public function posts()
    {
        return $this->belongsToMany('App\Models\Post');
    }
}
