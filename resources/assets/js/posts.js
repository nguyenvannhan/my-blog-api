import {Toast} from "./common";

var postsTable;

$(document).ready(function() {
    if ($("#posts-create-form").length) {
        $(".select2bs4").select2({
            theme: "bootstrap4",
            placeholder: "",
            allowClear: true
        });

        // editormd('content', {
        //     path: editorLibPath,
        //     toolbar: false,
        //     height: 700,
        //     imageUpload: true,
        //     imageFormats: ["jpg", "jpeg", "gif", "png", "bmp", "webp"],
        //     imageUploadURL: process.env.MIX_MEDIA_UPLOADER,
        // })

        $("#published_at").datetimepicker({
            format: "YYYY-MM-DD"
        });

        $('#posts-create-form').on('submit', function (e) {
            let textContent = $("#content").text();
            $('textarea[name="content"]').text(textContent);
        });
    } else {
        if (window.updatePost) {
            const toast = Toast("bottom");

            if (window.updatePost == "create") {
                toast.fire({
                    icon: "success",
                    title:
                        '<span class="ml-2">Create New Post Successfully</span>'
                });
            } else {
                toast.fire({
                    icon: "success",
                    title: '<span class="ml-2">Update Post Successfully</span>'
                });
            }
        }

        initDataTable();

        deletePost();
    }
});

function initDataTable() {
    postsTable = $("#posts-datatable").DataTable({
        paging: true,
        ordering: true,
        autoWidth: false,
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: {
            url: rootUrl + "/posts/data-table",
            method: "POST"
        },
        columns: [
            { data: "id", name: "id", className: "text-center" },
            { data: "title", name: "title" },
            {
                data: "categories",
                name: "categories",
                className: "text-center"
            },
            {
                data: "published_at",
                name: "published_at",
                className: "text-center"
            },
            {
                data: "action",
                name: "action",
                className: "text-center",
                orderable: false,
                searchable: false
            }
        ],
        order: [[0, "DESC"]]
    });
}

function deletePost() {
    $(document).on("click", ".btn-delete", function(e) {
        e.preventDefault();

        let id = $(this).data("id");

        Swal.fire({
            title: "<strong>Delete Post</strong>",
            icon: "info",
            html: "<span>Do you really want to delete post?</span>",
            showCancelButton: true,
            showConfirmButton: true,
            showLoaderOnConfirm: true,
            preConfirm: function() {
                const toast = Toast("bottom");
                return new Promise((resolve, reject) => {
                    $.ajax({
                        url: rootUrl + "/posts/" + id,
                        type: "DELETE"
                    })
                        .then(function(data) {
                            if (data.success) {
                                postsTable.ajax.reload(null, false);

                                toast.fire({
                                    icon: "success",
                                    title:
                                        '<span class="ml-2">Delete Post Successfully</span>'
                                });
                            } else {
                                toast.fire({
                                    icon: "error",
                                    title:
                                        '<span class="ml-2">Delete Post failed!</span>'
                                });
                            }
                        })
                        .catch(function(err) {
                            toast.fire({
                                icon: "error",
                                title:
                                    '<span class="ml-2">Delete Post failed!</span>'
                            });
                        });
                });
            }
        });
    });
}
