import {Toast} from "./common";

var categoriesTable;

$(document).ready(function() {
    if ($("#categories-create-form").length) {
        $(".select2bs4").select2({
            theme: "bootstrap4",
            placeholder: "",
            allowClear: true
        });
    } else {
        if (window.updateCategory) {
            const toast = Toast("bottom");

            if (window.updateCategory == "create") {
                toast.fire({
                    icon: "success",
                    title:
                        '<span class="ml-2">Create New Category Successfully</span>'
                });
            } else {
                toast.fire({
                    icon: "success",
                    title:
                        '<span class="ml-2">Update Category Successfully</span>'
                });
            }
        }

        initDataTable();

        deleteCategory();
    }
});

function initDataTable() {
    categoriesTable = $("#categories-datatable").DataTable({
        paging: true,
        ordering: true,
        autoWidth: false,
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: {
            url: rootUrl + "/categories/data-table",
            method: "POST"
        },
        columns: [
            { data: "id", name: "id", className: "text-center" },
            { data: "name", name: "name" },
            { data: "description", name: "description" },
            {
                data: "parent",
                name: "parent",
                className: "text-center",
                orderable: false
            },
            {
                data: "posts_subjects_count",
                name: "posts_subjects_count",
                className: "text-center",
                orderable: false,
                searchable: false
            },
            {
                data: "action",
                name: "action",
                className: "text-center",
                orderable: false,
                searchable: false
            }
        ],
        order: [[0, "DESC"]]
    });
}

function deleteCategory() {
    $(document).on("click", ".btn-delete", function(e) {
        e.preventDefault();

        let id = $(this).data("id");

        Swal.fire({
            title: "<strong>Delete Category</strong>",
            icon: "info",
            html: "<span>Do you really want to delete category?</span>",
            showCancelButton: true,
            showConfirmButton: true,
            showLoaderOnConfirm: true,
            preConfirm: function() {
                const toast = Toast("bottom");
                return new Promise((resolve, reject) => {
                    $.ajax({
                        url: rootUrl + "/categories/" + id,
                        type: "DELETE"
                    })
                        .then(function(data) {
                            if (data.success) {
                                categoriesTable.ajax.reload(null, false);

                                toast.fire({
                                    icon: "success",
                                    title:
                                        '<span class="ml-2">Delete Category Successfully</span>'
                                });
                            } else {
                                toast.fire({
                                    icon: "error",
                                    title:
                                        '<span class="ml-2">Delete Category failed!</span>'
                                });
                            }
                        })
                        .catch(function(err) {
                            toast.fire({
                                icon: "error",
                                title:
                                    '<span class="ml-2">Delete Category failed!</span>'
                            });
                        });
                });
            }
        });
    });
}
