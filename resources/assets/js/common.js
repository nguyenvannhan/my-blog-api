export const initSummerNote = selector => {
    $(selector).summernote({
        height: 200,
        toolbar: [
            // [groupName, [list of button]]
            ["style", ["bold", "italic", "underline", "clear"]],
            ["fontsize", ["fontsize"]],
            ["color", ["color"]]
        ]
    });
};

export const Toast = position => {
    return Swal.mixin({
        toast: true,
        position: position,
        showConfirmButton: false,
        timer: 5000
    });
};
