import { Toast } from "./common";

$(document).ready(function() {
    if ($("#upload-media-form").length) {
        $("#upload-media-form").trigger("reset");

        bsCustomFileInput.init();

        previewImage();

        resetForm();
    } else {
        if (window.uploadGallery) {
            const toast = Toast("bottom");

            toast.fire({
                icon: "success",
                title: '<span class="ml-2">Upload Gallery Successfully</span>'
            });
        }

        $(".media-item a.img-link").on("click", function(e) {
            e.preventDefault();

            let imgSrc = $(this)
                .find("img")
                .attr("src");

            window.prompt("Image URL:", imgSrc);
        });

        deleteImage();
    }
});

function previewImage() {
    $("#upload-media-form").on("change", "#inputGroupFile01", function(e) {
        readURL(this);
    });
}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $("#upload-image").attr("src", e.target.result);
        };

        reader.readAsDataURL(input.files[0]); // convert to base64 string
    }
}

function resetForm() {
    $("#upload-media-form").on("reset", function(e) {
        $("#upload-image").attr(
            "src",
            "https://icon-library.com/images/no-image-available-icon/no-image-available-icon-6.jpg"
        );
    });
}

function deleteImage() {
    $(document).on("click", ".btn-delete", function(e) {
        e.preventDefault();

        let id = $(this).data("id");

        Swal.fire({
            title: "<strong>Delete Image</strong>",
            icon: "info",
            html: "<span>Do you really want to delete image?</span>",
            showCancelButton: true,
            showConfirmButton: true,
            showLoaderOnConfirm: true,
            preConfirm: function() {
                const toast = Toast("bottom");
                return new Promise((resolve, reject) => {
                    $.ajax({
                        url: rootUrl + "/medias/" + id,
                        type: "DELETE"
                    })
                        .then(function(data) {
                            if (data.success) {
                                toast.fire({
                                    icon: "success",
                                    title:
                                        '<span class="ml-2">Delete Image Successfully</span>'
                                });

                                setTimeout(function() {
                                    window.location.reload();
                                }, 1000);
                            } else {
                                toast.fire({
                                    icon: "error",
                                    title:
                                        '<span class="ml-2">Delete Image failed!</span>'
                                });
                            }
                        })
                        .catch(function(err) {
                            toast.fire({
                                icon: "error",
                                title:
                                    '<span class="ml-2">Delete Image failed!</span>'
                            });
                        });
                });
            }
        });
    });
}
