$.ajaxSetup({
    headers: {
        "X-CSRF-TOKEN": $('meta[name="admin-lte-token"]').attr("content")
    }
});

if ($.fn.datetimepicker) {
    $.fn.datetimepicker.Constructor.Default = $.extend(
        {},
        $.fn.datetimepicker.Constructor.Default,
        {
            icons: {
                time: "fas fa-clock",
                date: "fas fa-calendar",
                up: "fas fa-arrow-up",
                down: "fas fa-arrow-down",
                previous: "fas fa-arrow-circle-left",
                next: "fas fa-arrow-circle-right",
                today: "far fa-calendar-check-o",
                clear: "fas fa-trash",
                close: "far fa-times"
            }
        }
    );
}

$(document).ready(function() {
    $("#btn-logout").on("click", function(e) {
        e.preventDefault();

        $("#form-logout").trigger("submit");
    });
});
