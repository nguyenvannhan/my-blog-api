@extends('layouts.master')

@section('title', $pageTitle.' Post')

@section('plugin_stylesheets')
    <link rel="stylesheet" href="{{ asset('vendors/admin-lte-3.0.5/plugins/select2/css/select2.min.css') }}"/>
    <link rel="stylesheet"
          href="{{ asset('vendors/admin-lte-3.0.5/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}"/>

    <!-- Tempusdominus Bbootstrap 4 -->
    <link rel="stylesheet"
          href="{{ asset('vendors/admin-lte-3.0.5/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}"/>

    <link rel="stylesheet" href="{{ asset('vendors/editor-md/css/editormd.css') }}"/>
@endsection


@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>
                        Posts
                    </h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('index') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('posts.index') }}">Posts</a></li>
                        <li class="breadcrumb-item active">{{ $pageTitle }}</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!-- Filter -->
                    <div class="card card-primary card-outline">
                        <div class="card-header">
                            <h3 class="card-title">
                                {{ $pageTitle }} Post
                            </h3>
                        </div>

                        <div class="card-body">
                            <form role="form" method="POST" action="{{ $formInfo['url'] }}" id="posts-create-form"
                                  enctype="multipart/form-data">
                                @csrf
                                @method($formInfo['method'])

                                <div class="row">
                                    @error('common')
                                    <div class="alert alert-danger col-12">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                    <div class="col-12 col-md-6">
                                        <div class="form-group">
                                            <label for="title">Title</label>
                                            <input class="form-control @error('title') is-invalid @enderror" id="title"
                                                   name="title" value="{{ $post->title }}"/>
                                            @error('title')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <div class="form-group">
                                            <label for="published_at">Publish Time</label>
                                            <input class="form-control @error('published_at') is-invalid @enderror"
                                                   name="published_at" id="published_at" data-toggle="datetimepicker"
                                                   data-target="#published_at" value="{{ $post->published_at }}"/>
                                            @error('published_at')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <div class="form-group">
                                            <label for="category_ids">Categories</label>
                                            <select
                                                class="form-control select2bs4 @error('category_ids') is-invalid @enderror"
                                                id="category_ids" name="category_ids[]" multiple>
                                                @foreach ($categoryList as $category)
                                                    <option value="{{ $category->id }}"
                                                        {{ $post->categories->contains('id', $category->id) ? 'selected' : '' }}>
                                                        {{ $category->name }}</option>
                                                @endforeach
                                            </select>
                                            @error('category_ids')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <div class="form-group">
                                            <label for="source_url">Source URL</label>
                                            <input type="text"
                                                   class="form-control @error('source_url') is-invalid @enderror"
                                                   id="source_url" name="source_url">
                                            @error('source_url')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="custom-file">
                                                    <input type="file"
                                                           class="custom-file-input @error('thumbnail') is-invalid @enderror"
                                                           id="inputGroupFile01" name="thumbnail">
                                                    <label class="custom-file-label"
                                                           for="inputGroupFile01">{{ $post->thumbnail_label }}</label>
                                                </div>
                                            </div>
                                            @error('thumbnail')
                                            <span class="invalid-feedback d-block" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="description">Description</label>
                                            <textarea name="description" id="description" rows="4"
                                                      class="form-control @error('description') is-invalid @enderror">{{ $post->description }}</textarea>
                                            @error('description')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label>Content</label>
                                            <div id="my-editor">
                                                {{--                                            <textarea name="content"--}}
                                                {{--                                                      class="form-control @error('content') is-invalid @enderror">{{ $post->content }}</textarea>--}}
                                                <my-editor :id-name="`content`"></my-editor>
                                            </div>
                                            <textarea style="display: none"
                                                      name="content">{{ $post->content }}</textarea>
                                            @error('content')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-12 text-center">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                        <a href="{{ route('posts.index') }}" type="button"
                                           class="btn btn-warning">Cancel</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('plugin_scripts')
    <script src="{{ asset('vendors/admin-lte-3.0.5/plugins/moment/moment.min.js') }}"></script>

    <!-- Select2 -->
    <script src="{{ asset('vendors/admin-lte-3.0.5/plugins/select2/js/select2.full.min.js') }}"></script>

    <script
        src="{{ asset('vendors/admin-lte-3.0.5/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}">
    </script>

    <script
        src="{{ asset('vendors/admin-lte-3.0.5/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>

    <script src="{{ asset('vendors/editor-md/editormd.min.js') }}"></script>
    <script>
        var editorLibPath = "{{ asset('vendors/editor-md/lib/') }}/";
    </script>

    <script>
        {{--    hljs.initHighlightingOnLoad();--}}
        bsCustomFileInput.init();
    </script>
    <script src="{{ mix('js/editor.js') }}"></script>
@endsection


@section('scripts')
    <script src="{{ mix('js/posts.js') }}"></script>
@endsection
