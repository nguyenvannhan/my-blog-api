@extends('layouts.master')

@section('title', 'Upload Media')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>
                    Medias
                </h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('index') }}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('medias.index') }}">Meidas</a></li>
                    <li class="breadcrumb-item active">Upload</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<section class="content">
    <div class="container-fluid">
        <form action="{{ route('medias.store') }}" method="POST" enctype="multipart/form-data" id="upload-media-form">
            @csrf
            <div class="row">
                @if($errors->any())
                <div class="col-12 alert alert-danger">
                    @foreach($errors->all() as $errMessage)
                    <p>{{ $errMessage }}</p>
                    @endforeach
                </div>
                @endif
                <div class="col-12 col-md-6 mb-3 mb-md-0">
                    <img class="img-fluid" id="upload-image"
                        src="https://icon-library.com/images/no-image-available-icon/no-image-available-icon-6.jpg" />
                </div>
                <div class="col-12 col-md-6">
                    <div class="input-group mb-3">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="inputGroupFile01" name="file">
                            <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                        </div>
                    </div>
                    <div>
                        <button class="btn btn-primary mr-2" type="submit">Upload</button>
                        <button class="btn btn-warning mr-2" type="reset">Reset</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
@endsection


@section('plugin_scripts')
<script src="{{ asset('vendors/admin-lte-3.0.5/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
@endsection


@section('scripts')
<script src="{{ mix('js/medias.js') }}"></script>
@endsection
