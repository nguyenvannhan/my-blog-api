@extends('layouts.master')

@section('title', 'Media Management')

@section('plugin_stylesheets')
<!-- SweetAlert2 -->
<link rel="stylesheet"
    href="{{ asset('vendors/admin-lte-3.0.5/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
@endsection

@section('stylesheets')
<style>
    .media-item>.btn-delete {
        position: absolute;
        top: 0.5rem;
        right: 0.5rem;
        display: none;
    }

    .media-item:hover>.btn-delete {
        display: inline-block;
    }
</style>
@endsection

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>
                    Media <a href="{{ route('medias.create') }}" class="btn btn-success btn-xs">Upload New</a>
                </h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
                    <li class="breadcrumb-item active">Media</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 mb-3">
                <div class="d-grid x12 g-3">
                    @foreach($galleryList as $galleryItem) <div
                        class="g-col-12 g-col-md-3 rounded border border-gray media-item position-relative">
                        <a href="#" class="img-link">
                            <div class="media-wrapper-3x2">
                                <img src="{{ $galleryItem->getUrl() }}" alt="" class="img-cover">
                            </div>
                        </a>
                        <a href="#" data-id="{{ $galleryItem->id }}" class="btn btn-delete btn-danger">
                            <i class="fas fa-times-circle"></i>
                        </a>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="col-12">
                <div class="d-flex justify-content-center">
                    {!! $galleryList->links() !!}
                </div>
            </div>
        </div>
</section>
@endsection


@section('plugin_scripts')
<!-- SweetAlert2 -->
<script src="{{ asset('vendors/admin-lte-3.0.5/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
@endsection

@section('scripts')
@if(session('uploadGallery'))
<script>
    window.uploadGallery = true;
</script>
@endif
<script src="{{ mix('js/medias.js') }}"></script>
@endsection
