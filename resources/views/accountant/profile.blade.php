<div class="row">
    <div class="col-12 d-block d-md-flex justify-content-between">
        <p>Profile: <strong>{{ $profile->name }} (#{{ $profile->id }})</strong></p>
        <p>Tổng: <strong class="text-danger">{{ number_format($feeList->sum('money'), 0, '.', ',') }} VNĐ</strong></p>
    </div>
    <div class="col-12">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr class="text-center font-weight-bold">
                        <td>Time</td>
                        <td>Số tiền</td>
                        <td>Note</td>
                    </tr>
                </thead>
                <tbody style="overflow-y: auto; max-height: 60vh;">
                    @foreach($feeList as $fee)
                    <tr>
                        <td class="text-center">{{ $fee->created_at }}</td>
                        <td class="text-center">{{ number_format($fee->money, 0, '.', ',') }} VNĐ</td>
                        <td class="text-center">{!! $fee->note !!}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
