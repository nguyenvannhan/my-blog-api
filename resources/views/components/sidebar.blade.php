<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ route('index') }}" class="brand-link text-center">
        <span class="brand-text font-weight-light"><strong>GiaLang's Blog</strong></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                @php $segment = Request()->segment(1); @endphp
                <li class="nav-item has-treeview">
                    <a href="{{ route('dashboard') }}" class="nav-link {{ $segment == 'dashboard' ? 'active' : '' }}">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Dashboard
                        </p>
                    </a>
                </li>

                <li class="nav-item has-treeview">
                    <a href="{{ route('medias.index') }}" class="nav-link {{ $segment == 'medias' ? 'active' : '' }}">
                        <i class="nav-icon fas fa-photo-video"></i>
                        <p>
                            Media
                        </p>
                    </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="{{ route('posts.index') }}" class="nav-link {{ $segment == 'posts' ? 'active' : '' }}">
                        <i class="nav-icon fas fa-newspaper"></i>
                        <p>
                            Posts
                        </p>
                    </a>
                </li>

                <li class="nav-item has-treeview">
                    <a href="{{ route('categories.index') }}"
                        class="nav-link {{ $segment == 'categories' ? 'active' : '' }}">
                        <i class="nav-icon fas fa-paw"></i>
                        <p>
                            Categories
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</aside>
