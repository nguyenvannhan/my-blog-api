@extends('layouts.master')

@section('title', 'Categories Management')


@section('plugin_stylesheets')
<!-- DataTables -->
<link rel="stylesheet"
    href="{{ asset('vendors/admin-lte-3.0.5/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet"
    href="{{ asset('vendors/admin-lte-3.0.5/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<!-- SweetAlert2 -->
<link rel="stylesheet"
    href="{{ asset('vendors/admin-lte-3.0.5/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
@endsection

@section('stylesheets')
@endsection

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>
                    Categories <a href="{{ route('categories.create') }}" class="btn btn-success btn-xs">Add New</a>
                </h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
                    <li class="breadcrumb-item active">Categories</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<section class="content">
    <div class="container-fluid">
        <!-- DataTable -->
        <div class="card">
            <div class="card-body">
                <table id="categories-datatable" class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr class="text-center">
                            <th>ID</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Parent</th>
                            <th>Posts/Subjects Count</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    <tfoot>
                        <tr class="text-center">
                            <th>ID</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Parent</th>
                            <th>Posts/Subjects Count</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <!-- End DataTable -->
    </div>
</section>
@endsection


@section('plugin_scripts')
<!-- DataTables -->
<script src="{{ asset('vendors/admin-lte-3.0.5/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendors/admin-lte-3.0.5/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('vendors/admin-lte-3.0.5/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}">
</script>
<script src="{{ asset('vendors/admin-lte-3.0.5/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}">
</script>
<!-- SweetAlert2 -->
<script src="{{ asset('vendors/admin-lte-3.0.5/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
@endsection


@section('scripts')
@if(session('updateCategory'))
<script>
    window.updateCategory = "{{ session('updateCategory') }}";
</script>
@endif
<script src="{{ mix('js/categories.js') }}"></script>
@endsection
