@extends('layouts.master')

@section('title', 'Profile | English Homestay');

@section('plugin_stylesheets')
<!-- Tempusdominus Bbootstrap 4 -->
<link rel="stylesheet"
    href="{{ asset('vendors/admin-lte-3.0.5/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendors/admin-lte-3.0.5/plugins/select2/css/select2.min.css') }}">
<link rel="stylesheet"
    href="{{ asset('vendors/admin-lte-3.0.5/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">

<!-- DataTables -->
<link rel="stylesheet"
    href="{{ asset('vendors/admin-lte-3.0.5/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet"
    href="{{ asset('vendors/admin-lte-3.0.5/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<!-- SweetAlert2 -->
<link rel="stylesheet"
    href="{{ asset('vendors/admin-lte-3.0.5/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
<!-- summernote -->
<link rel="stylesheet" href="{{ asset('vendors/admin-lte-3.0.5/plugins/summernote/summernote-bs4.css') }}">
@endsection

@section('stylesheets')
<style>
    #submit-btn-search {
        margin-bottom: 1rem;
    }

    @media (min-width: 768px) {

        #submit-btn-search,
        #reset-btn-search {
            margin-top: 2rem;
        }

        #submit-btn-search {
            margin-bottom: 0;
        }
    }

    .btn-status:not(last-of-type) {
        margin-right: 0.5rem;
    }

    .badge-0 {
        color: #fff;
        background-color: #28a745;
    }

    .badge-1 {
        color: #fff;
        background-color: #17a2b8;
    }

    .badge-2,
    .badge-4 {
        color: #fff;
        background-color: #007bff;
    }

    .badge-3 {
        color: #fff;
        background-color: #dc3545;
    }

    .badge-5,
    .badge-6 {
        color: #fff;
        background-color: #ffc107;
    }
</style>
@endsection

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>
                    Profile
                </h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('index') }}">Home</a></li>
                    <li class="breadcrumb-item active">Profile</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!-- Filter -->
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fas fa-edit"></i>
                            Filter
                        </h3>
                    </div>
                    <div class="card-body">
                        <form role="form" id="profile-filter">
                            <div class="row">
                                <div class="col-12 col-md-4">
                                    <div class="form-group">
                                        <label for="from-date">From Date</label>
                                        <div class="input-group date" id="from-date" data-target-input="nearest">
                                            <input type="text" class="form-control datetimepicker-input"
                                                data-target="#from-date" />
                                            <div class="input-group-append" data-target="#from-date"
                                                data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-4">
                                    <div class="form-group">
                                        <label for="to-date">To Date</label>
                                        <div class="input-group date" id="to-date" data-target-input="nearest">
                                            <input type="text" class="form-control datetimepicker-input"
                                                data-target="#to-date" />
                                            <div class="input-group-append" data-target="#to-date"
                                                data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-4">
                                    <div class="form-group">
                                        <label for="marketer-id">Marketing</label>
                                        <select class="form-control select2bs4" id="marketer-id" style="width: 100%;">
                                            <option value="">Danh sách Marketers</option>
                                            @foreach($marketerList as $marketer)
                                            <option value="{{ $marketer->id }}">{{ $marketer->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12 col-md-8">
                                    <div class="form-group">
                                        <label for="phone">Phone</label>
                                        <input type="text" class="form-control" id="phone"
                                            placeholder="Mỗi SĐT cách nhau bới dấu , hoặc ;">
                                    </div>
                                </div>
                                <div class="col-12 col-md-4">
                                    <div class="row">
                                        <div class="col-12 col-md-6">
                                            <button id="submit-btn-search" type="submit"
                                                class="btn btn-primary btn-block">Submit</button>
                                        </div>
                                        <div class="col-12 col-md-6">
                                            <button id="reset-btn-search" type="button"
                                                class="btn btn-warning btn-block">Reset</button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- End Filter -->

                <!-- Status List -->
                <div class="card card-danger card-outline">
                    <div class="card-body pb-0">
                        <a href="{{ route('profile.index') }}" class="btn btn-info btn-status mb-4">Tất cả</a>
                        @foreach(\App\Models\Profile::STATUS_LIST as $key => $status)
                        <a href="{{ route('profile.index', ['status_id' => $key]) }}"
                            class="btn btn-info btn-status mb-4">{{ $status }}</a>
                        @endforeach
                    </div>
                </div>
                <!-- End Status List -->

                <!-- DataTable -->
                <div class="card">
                    <div class="card-body">
                        <table id="profile-datatable" class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr class="text-center">
                                    <th>ID</th>
                                    <th>Profile Info</th>
                                    <th>Lịch hẹn</th>
                                    <th>Note</th>
                                    <th>Status</th>
                                    <th>Author</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            <tfoot>
                                <tr class="text-center">
                                    <th>ID</th>
                                    <th>Profile Info</th>
                                    <th>Lịch hẹn</th>
                                    <th>Note</th>
                                    <th>Status</th>
                                    <th>Author</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <!-- End DataTable -->
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-edit">
        <div class="modal-dialog modal-xl modal-dialog-centered">
            <div class="modal-content">
                <form id="edit-profile-form">
                    <div id="overlay" class="overlay d-none justify-content-center align-items-center">
                        <i class="fas fa-2x fa-sync fa-spin"></i>
                    </div>
                    <div class="modal-header">
                        <h4 class="modal-title">Edit Profile</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="modal-fee">
        <div class="modal-dialog modal-xl modal-dialog-centered">
            <div class="modal-content">
                <form id="fee-profile-form">
                    <div id="overlay" class="overlay d-none justify-content-center align-items-center">
                        <i class="fas fa-2x fa-sync fa-spin"></i>
                    </div>
                    <div class="modal-header">
                        <h4 class="modal-title">Đóng Học Phí</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="profile_id" value="">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label>Số tiền</label>
                                    <input class="form-control" name="money" type="text">
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <label>Notes</label>
                                    <textarea class="form-control" id="fee-profile-explain" name="explain"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>
    <!-- /.modal-dialog -->

    <div class="modal fade" id="modal-fee-detail">
        <div class="modal-dialog modal-xl modal-dialog-centered">
            <div class="modal-content">
                <form id="fee-profile-form">
                    <div id="overlay" class="overlay d-none justify-content-center align-items-center">
                        <i class="fas fa-2x fa-sync fa-spin"></i>
                    </div>
                    <div class="modal-header">
                        <h4 class="modal-title">Chi tiết Học Phí</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</section>


@endsection

@section('plugin_scripts')
<script src="{{ asset('vendors/admin-lte-3.0.5/plugins/moment/moment.min.js') }}"></script>

<!-- DataTables -->
<script src="{{ asset('vendors/admin-lte-3.0.5/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendors/admin-lte-3.0.5/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('vendors/admin-lte-3.0.5/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}">
</script>
<script src="{{ asset('vendors/admin-lte-3.0.5/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}">
</script>

<script
    src="{{ asset('vendors/admin-lte-3.0.5/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}">
</script>
<!-- Select2 -->
<script src="{{ asset('vendors/admin-lte-3.0.5/plugins/select2/js/select2.full.min.js') }}"></script>

<!-- SweetAlert2 -->
<script src="{{ asset('vendors/admin-lte-3.0.5/plugins/sweetalert2/sweetalert2.min.js') }}"></script>

<!-- Summernote -->
<script src="{{ asset('vendors/admin-lte-3.0.5/plugins/summernote/summernote-bs4.min.js') }}"></script>
@endsection

@section('scripts')
@if(session('create_profile'))
<script>
    window.createProfile = true;
</script>
@endif
<script src="{{ mix('js/profile.js') }}"></script>
@endsection
