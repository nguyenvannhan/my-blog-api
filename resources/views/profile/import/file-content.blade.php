@if($cityList->isEmpty())
<div class="alert alert-danger">
    @foreach($emailErrors as $emailErr)
    <p>Email <strong>{{ $emailErr }}</strong> đã tồn tại hoặc lặp lại trong list</p>
    @endforeach
    @foreach($phoneErrors as $phoneErr)
    <p>SĐT <strong>{{ $phoneErr }}</strong> đã tồn tại hoặc lặp lại trong list</p>
    @endforeach
</div>
@endif


<div class="w-100 table-responsive">
    <table class="table table-bordered table-stripped" id="file-content-table">
        <thead class="text-center">
            <th>#</th>
            <th>Họ tên</th>
            <th>Email</th>
            <th>SĐT</th>
            <th>Địa chỉ</th>
            <th>Notes</th>
        </thead>
        <tbody>
            @foreach ($profileList as $profile)
            <tr>
                <td class="text-center">{{ $loop->iteration }}</td>
                <td>{{ $profile->name }}</td>
                <td class="{{ $profile->email_class }}">{{ $profile->email }}</td>
                <td>{!! $profile->phone_html !!}</td>
                <td>{{ $profile->address }}</td>
                <td> {!! $profile->note !!}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

@if($cityList->isNotEmpty())
<div class="w-100">
    <div class="row">
        <div class="col-md-4">
            <label>Tỉnh/Thành phố</label>
            <select class="form-control" name="city_id" id="city">
                <option value="0">Lựa chọn Tỉnh/Thành phố</option>
                @foreach ($cityList as $city)
                <option value="{{ $city->id }}">{{ $city->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-4">
            <label>Quận/Huyện</label>
            <select class="form-control" name="district_id" id="district">
                <option value="0">Lựa chọn Quận/Huyện</option>
            </select>
        </div>
        <div class="col-md-4">
            <label><small class="font-italic">Click to save</small></label>
            <button class="btn btn-primary btn-block" id="import-save">Save</button>
        </div>
    </div>
</div>
@endif
