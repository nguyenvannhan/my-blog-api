@extends('layouts.master')

@section('title', 'Change Password | English HomeStay')

@section('stylesheets')
<style>
    #submit-btn-search {
        margin-bottom: 1rem;
    }

    @media (min-width: 768px) {

        #submit-btn-search,
        #reset-btn-search {
            margin-top: 2rem;
        }

        #submit-btn-search {
            margin-bottom: 0;
        }
    }
</style>
@endsection

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>
                    Change Password
                </h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('index') }}">Home</a></li>
                    <li class="breadcrumb-item active">Password</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary card-outline">
                    <div class="card-body">
                        <form role="form" action="{{ route('change_password') }}" method="POST">
                            @csrf
                            <div class="row">
                                @if($errors->any())
                                <div class="col-12 mb-3 text-danger">
                                    @foreach($errors->all() as $err)
                                    <span>{{ $err }}</span>
                                    @if(!$loop->last)
                                    <br>
                                    @endif
                                    @endforeach
                                </div>
                                @endif
                                <div class="col-12 col-md-3">
                                    <div class="form-group">
                                        <label for="from-date">Current Password</label>
                                        <input class="form-control" name="current_password" type="password" required>
                                    </div>
                                </div>
                                <div class="col-12 col-md-3">
                                    <div class="form-group">
                                        <label for="from-date">New Password</label>
                                        <input class="form-control" name="password" type="password" required>
                                    </div>
                                </div>
                                <div class="col-12 col-md-3">
                                    <div class="form-group">
                                        <label for="from-date">Confirm New Password</label>
                                        <input class="form-control" name="password_confirmation" type="password" required>
                                    </div>
                                </div>
                                <div class="col-12 col-md-3">
                                    <button id="submit-btn-search" type="submit"
                                    class="btn btn-primary btn-block">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        @endsection
