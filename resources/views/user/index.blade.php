@extends('layouts.master')

@section('title', 'Users | English Homestay');

@section('plugin_stylesheets')
<!-- DataTables -->
<link rel="stylesheet"
    href="{{ asset('vendors/admin-lte-3.0.5/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet"
    href="{{ asset('vendors/admin-lte-3.0.5/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<!-- SweetAlert2 -->
<link rel="stylesheet"
    href="{{ asset('vendors/admin-lte-3.0.5/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendors/admin-lte-3.0.5/plugins/select2/css/select2.min.css') }}">
<link rel="stylesheet"
    href="{{ asset('vendors/admin-lte-3.0.5/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
@endsection

@section('stylesheets')
<style>
    .badge-1 {
        color: #fff;
        background-color: #17a2b8;
    }

    .badge-2 {
        color: #fff;
        background-color: #007bff;
    }

    #submit-btn-search {
        margin-bottom: 1rem;
    }

    @media (min-width: 768px) {

        #submit-btn-search,
        #reset-btn-search {
            margin-top: 2rem;
        }

        #submit-btn-search {
            margin-bottom: 0;
        }
    }
</style>
@endsection

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>
                    Users <a href="{{ route('user.create') }}" class="btn btn-success btn-xs">Add New</a>
                </h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('index') }}">Home</a></li>
                    <li class="breadcrumb-item active">Users</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!-- Filter -->
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fas fa-edit"></i>
                            Filter
                        </h3>
                    </div>
                    <div class="card-body">
                        <form role="form" id="user-filter">
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="search-keyword">Search Keyword</label>
                                        <input type="text" name="search_keyword" id="search-keyword"
                                            class="form-control">
                                    </div>
                                </div>
                                <div class="col-12 col-md-3">
                                    <div class="form-group">
                                        <label for="permission-ids">Role</label>
                                        <select class="form-control select2bs4" multiple id="permission-ids"
                                            name="permission_ids[]" style="width: 100%;">
                                            @foreach($roleList as $role)
                                            @foreach($role->permissions as $permission)
                                            <option value="{{ $permission->id }}">
                                                {{ ucwords(__('permissions.display_name.'.$permission->name)) }}
                                            </option>
                                            @endforeach
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12 col-md-3">
                                    <div class="row">
                                        <div class="col-12 col-md-6">
                                            <button id="submit-btn-search" type="submit"
                                                class="btn btn-primary btn-block">Submit</button>
                                        </div>
                                        <div class="col-12 col-md-6">
                                            <button id="reset-btn-search" type="button"
                                                class="btn btn-warning btn-block">Reset</button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- End Filter -->

                <!-- DataTable -->
                <div class="card">
                    <div class="card-body">
                        <table id="user-datatable" class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr class="text-center">
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Roles - Permissions</th>
                                    <th>Status</th>
                                    <th>Timestamps</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                                <tr class="text-center">
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Roles - Permissions</th>
                                    <th>Status</th>
                                    <th>Timestamps</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <!-- End DataTable -->
            </div>
        </div>
    </div>
</section>
@endsection


@section('plugin_scripts')
<script src="{{ asset('vendors/admin-lte-3.0.5/plugins/moment/moment.min.js') }}"></script>

<!-- DataTables -->
<script src="{{ asset('vendors/admin-lte-3.0.5/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendors/admin-lte-3.0.5/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('vendors/admin-lte-3.0.5/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}">
</script>
<script src="{{ asset('vendors/admin-lte-3.0.5/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}">
</script>
<!-- SweetAlert2 -->
<script src="{{ asset('vendors/admin-lte-3.0.5/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('vendors/admin-lte-3.0.5/plugins/select2/js/select2.full.min.js') }}"></script>
@endsection

@section('scripts')
@if(session('create_user'))
<script>
    window.createUser = true;
</script>
@endif
@if(session('update_user'))
<script>
    window.updateUser = true;
</script>
@endif
<script src="{{ mix('js/user.js') }}"></script>
@endsection
