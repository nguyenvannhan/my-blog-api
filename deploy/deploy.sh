#!/usr/bin/env bash

php artisan migrate --force

php artisan cache:clear

php artisan auth:clear-resets

php artisan route:cache
php artisan view:cache
php artisan config:cache

find . -type f -exec chmod 644 {} \;
find . -type d -exec chmod 755 {} \;
