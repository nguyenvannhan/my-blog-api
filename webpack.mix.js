const mix = require("laravel-mix");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass("resources/assets/sass/app.scss", "public/css")

    .js('resources/assets/js/editor.js', 'public/js')
    .js("resources/assets/js/app.js", "public/js")
    .js("resources/assets/js/categories.js", "public/js")
    .js("resources/assets/js/medias.js", "public/js")
    .js("resources/assets/js/posts.js", "public/js")
    .js("resources/assets/js/subjects.js", "public/js")

    .copyDirectory("resources/assets/vendors", "public/vendors")

    .options({ processCssUrls: false });

if (mix.inProduction()) {
    mix.version();
}
