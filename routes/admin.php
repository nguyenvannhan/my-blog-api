<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\SubjectController;
use Illuminate\Support\Facades\Route;

Route::get('/change-password', 'AuthController@changePassword')->name('change_password');
    Route::post('/change-password', 'AuthController@postChangePassword');

Route::get('/', [DashboardController::class, 'index'])->name('index');

Route::resources([
    'posts' => PostController::class,
    'categories' => CategoryController::class,
    'subjects' => SubjectController::class
]);
