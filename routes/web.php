<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\MediaController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\SubjectController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes([
    'register' => false,
    'reset' => false
]);

Route::get('/', function () {
    return view('welcome');
})->name('index');
Route::middleware('auth')->group(function () {
    Route::get('/change-password', [AuthController::class, 'changePassword'])->name('change_password');
    Route::post('/change-password', [AuthController::class, 'postChangePassword']);

    Route::redirect('/admin', '/dashboard', 301);
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');

    Route::post('medias/editor-upload', [MediaController::class, 'editorUpload']);

    Route::resources([
        'categories' => CategoryController::class,
        'medias' => MediaController::class,
        'posts' => PostController::class,
        'subjects' => SubjectController::class
    ]);

    Route::prefix('categories')->name('categories.')->group(function () {
        Route::post('data-table', [CategoryController::class, 'getTableData']);
    });

    Route::prefix('posts')->name('posts.')->group(function () {
        Route::post('data-table', [PostController::class, 'getTableData']);
    });
});
